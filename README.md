# Auto-daylight

Control IKEA Trådfri lamps with a web-app for fun and profit

# Installation

First install libcoap by running this script:

    #!/bin/sh
    git clone --depth 1 --recursive -b dtls https://github.com/home-assistant/libcoap.git
    cd libcoap
    ./autogen.sh
    ./configure --disable-documentation --disable-shared --without-debug CFLAGS="-D COAP_DEBUG_FD=stderr"
    make
    make install

Then clone this repo, install the requirements and run main.py.

from pytradfri import Gateway
from pytradfri.api.libcoap_api import APIFactory
from pytradfri.util import load_json
from pytradfri.error import RequestTimeout


def query_all_light_levels(psk_conf):
    devices, _ = _connect_to_gateway(psk_conf)

    lights = [dev for dev in devices if dev.has_light_control]

    if lights:
        result = [{
            'id': idx,
            'name': light.name,
            'state': light.light_control.lights[0].state,
            'brightness': light.light_control.lights[0].dimmer,
            'colortemp': light.light_control.lights[0].color_temp,
        } for idx, light in enumerate(lights, 1)]
        return result
    raise RuntimeError("No lights found")


def change_light_levels(light_name, dimmer, color_temp, psk_conf):
    devices, api = _connect_to_gateway(psk_conf)

    lights = [dev for dev in devices if dev.has_light_control]

    # print(lights)

    light = None
    if lights:
        for potential_light in lights:
            if potential_light.name == light_name:
                light = potential_light

    if light:
        # checks state of the light (true=on)
        print("State: {}".format(light.light_control.lights[0].state))

        # get dimmer level of the light
        print("Dimmer: {}".format(light.light_control.lights[0].dimmer))

        # get color temp level of the light
        print("Color temp: {}".format(light.light_control.lights[0].color_temp))

        # get the name of the light
        print("Name: {}".format(light.name))

        # set the light level of the light
        if dimmer:
            dim_command = light.light_control.set_dimmer(int(dimmer))
            try:
                api(dim_command, timeout=5)
            except RequestTimeout:
                raise RuntimeError("Timed out setting dimmer for light {}".format(light.name))

        # change color of the light
        # f5faf6 = cold | f1e0b5 = normal | efd275 = warm
        if color_temp:
            color_command = light.light_control.set_color_temp(int(color_temp))
            try:
                api(color_command, timeout=5)
            except RequestTimeout:
                raise RuntimeError("Timed out setting color temp for light {}".format(light.name))
    else:
        raise RuntimeError('No light named {} found!'.format(light_name))


def load_psk_conf():
    try:
        conf = load_json('tradfri_standalone_psk.conf')
        return conf
    except Exception as e:
        raise RuntimeError('Error loading psk conf: {}'.format(str(e)))


def _connect_to_gateway(psk_conf):
    host = list(psk_conf.keys())[0]
    identity = psk_conf[host]['identity']
    psk = psk_conf[host]['key']
    api_factory = APIFactory(host=host, psk_id=identity, psk=psk)
    api = api_factory.request
    gateway = Gateway()
    devices_command = gateway.get_devices()
    try:
        devices_commands = api(devices_command, timeout=5)
        devices = api(devices_commands, timeout=5)
    except RequestTimeout:
        raise RuntimeError('Timed out connecting to gateway {}'.format(host))
    return devices, api


function showError(msg) {
    console.error(msg)
    const errorMsgElem = document.querySelector('.error-msg')
    errorMsgElem.classList.remove('hidden')
    errorMsgElem.innerHTML = msg
}

function resetError() {
    const errorMsgElem = document.querySelector('.error-msg')
    errorMsgElem.classList.add('hidden')
    errorMsgElem.innerHTML = null
}

function getLights() {
    return fetch('/api/lights')
    .then((response) => {
        if (response.status !== 200) {
            console.error("getLights response: ", response)
            response.json()
                .then((jsonBody) => showError(`${jsonBody.error || "Unknown error"}`))
                .catch(() => showError(`${response.status} ${response.statusText}`))
        } else {
            return response.json()
        }
    })
    .catch(err => showError(err))
}

function changeBrightness(event, lightName, lightId) {
    const brightness = event.srcElement.value
    console.log(`Change brightness to ${brightness} for ${lightName}`)
    fetch(`/api/light/${lightName}/brightness/${brightness}`, {method: "POST"})
    .then((response)  => {
        if (response.status !== 200) {
            showError(`Error ${response.status}: ${response.statusText}`)
            console.error("changeBrightness response: ", response)
        } else {
            document.querySelector(`#brightness-${lightId}-label`).innerHTML = brightness
        }
    })
    .catch(err => showError(err))
}

function changeColorTemperature(event, lightName, lightId) {
    const colorTemp = event.srcElement.value
    console.log(`Change color temp to ${colorTemp} for ${lightName}`)
    fetch(`/api/light/${lightName}/colortemp/${colorTemp}`, {method: "POST"})
    .then((response)  => {
        if (response.status !== 200) {
            showError(`Error ${response.status}: ${response.statusText}`)
            console.error("changeColorTemperature response: ", response)
        } else {
            document.querySelector(`#colortemp-${lightId}-label`).innerHTML = colorTemp
        }
    })
    .catch(err => showError(err))
}

function renderLights(lights) {
    let outputHtml = ''
    lights.sort((el1, el2) => el1.name > el2.name).map((light, idx) => {
        const elem = `
            <div class="lamp-control">
                <p>${light.name} (${light.state ? 'on' : 'off'})</p>

                <div>
                  <input type="range" id="brightness-${light.id}" name="brightness"
                         min="0" max="254" value="${light.brightness}" step="1"
                         onchange="changeBrightness(event, '${light.name}', ${light.id})">
                  <span id="brightness-${light.id}-label">${light.brightness}</span>
                  <label for="brightness-${light.id}">Brightness</label>
                </div>

                <div>
                  <input type="range" id="colortemp-${light.id}" name="colortemp"
                         min="250" max="454" value="${light.colortemp}" step="1"
                         onchange="changeColorTemperature(event, '${light.name}', ${light.id})">
                  <span id="colortemp-${light.id}-label">${light.colortemp}</span>
                  <label for="colortemp-${light.id}">Color temperature</label>
                </div>
            </div>
        `
        outputHtml += elem
    })

    const lampControls = document.querySelector('.lamp-controls')
    lampControls.innerHTML = outputHtml
    lampControls.classList.remove("hidden")
}

getLights()
    .then(lights => lights && renderLights(lights))
    .catch(err => showError(err))
    .finally(() => document.querySelector('.lamp-controls').classList.remove('hidden'))
from flask import Flask, render_template, jsonify, make_response
from json import dumps

from control_lights import load_psk_conf, query_all_light_levels, change_light_levels

app = Flask(__name__)
conf = load_psk_conf()


@app.route("/")
def index():
    return render_template("index.html")


@app.route("/api/light/<light>/brightness/<brightness>", methods=['POST'])
def change_brightness(light, brightness):
    try:
        change_light_levels(light, brightness, None, conf)
    except RuntimeError as e:
        return jsonify({"error": str(e)})
    return jsonify({"status": "OK", "light": light, "brightness": brightness})


@app.route("/api/light/<light>/colortemp/<colortemp>", methods=['POST'])
def change_color_temp(light, colortemp):
    try:
        change_light_levels(light, None, colortemp, conf)
    except RuntimeError as e:
        return jsonify({"error": str(e)})
    return jsonify({"status": "OK", "light": light, "colortemp": colortemp})


@app.route("/api/lights")
def get_lights():
    try:
        lights = query_all_light_levels(conf)
        return jsonify(lights)
    except RuntimeError as e:
        error_msg_json = dumps({'error': str(e)})
        return make_response((error_msg_json, 500))


app.run(host="0.0.0.0", port=8060)
